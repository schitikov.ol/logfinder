package com.linefight.logfinder;

import com.linefight.logfinder.app.service.LogfinderService;
import com.linefight.logfinder.config.RootContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RootContextConfiguration.class)
public class ContextLoadedTest {

    @Autowired
    LogfinderService service;

    @Test
    public void contextLoaded() {

    }

}
