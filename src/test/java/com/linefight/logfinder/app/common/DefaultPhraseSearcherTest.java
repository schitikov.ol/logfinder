package com.linefight.logfinder.app.common;

import org.junit.Before;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefaultPhraseSearcherTest {

    private DefaultPhraseSearcher searcher;

    @Before
    public void setUp() throws URISyntaxException {
        searcher = new DefaultPhraseSearcher("test", Paths.get(getClass().getResource("/logs/test_log1.log").toURI()));
    }

    @Test
    public void nextMatchIndex() {
        assertEquals(0, searcher.nextMatchIndex());
        assertEquals(5, searcher.nextMatchIndex());
        assertEquals(9, searcher.nextMatchIndex());
        assertEquals(22, searcher.nextMatchIndex());
        assertEquals(35, searcher.nextMatchIndex());
        assertEquals(35, searcher.nextMatchIndex());
    }

    @Test
    public void prevMatchIndex() {
        searcher.nextMatchIndex();
        searcher.nextMatchIndex();
        searcher.nextMatchIndex();
        searcher.nextMatchIndex();
        searcher.nextMatchIndex();

        assertEquals(22, searcher.prevMatchIndex());
        assertEquals(9, searcher.prevMatchIndex());
        assertEquals(5, searcher.prevMatchIndex());
        assertEquals(0, searcher.prevMatchIndex());
        assertEquals(0, searcher.prevMatchIndex());
        assertEquals(0, searcher.prevMatchIndex());
    }

    @Test
    public void randomTraverse() {
        assertEquals(0, searcher.nextMatchIndex());
        assertEquals(5, searcher.nextMatchIndex());
        assertEquals(9, searcher.nextMatchIndex());
        assertEquals(5, searcher.prevMatchIndex());
        assertEquals(9, searcher.nextMatchIndex());
        assertEquals(5, searcher.prevMatchIndex());
        assertEquals(0, searcher.prevMatchIndex());

    }

    @Test
    public void allMatchIndices() {
        List<Integer> indices = searcher.allMatchIndices();

        assertEquals(5, indices.size());
        assertEquals(0, searcher.nextMatchIndex());
    }

    @Test
    public void hasMatch() {
        assertTrue(searcher.hasMatch());

        assertEquals(0, searcher.nextMatchIndex());
    }
}