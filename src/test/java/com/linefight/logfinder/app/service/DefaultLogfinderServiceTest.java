package com.linefight.logfinder.app.service;

import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DefaultLogfinderServiceTest {

    private LogfinderService service = new DefaultLogfinderService();

    @Test
    public void findFilesByExtension() {
        List<Path> files = service.findFilesByExtension(new File(getClass().getResource("/logs").getPath()).toString(), "log");
        assertNotNull(files);
        assertEquals(4, files.size());
    }
}