package com.linefight.logfinder.app.model;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FileTree {

    private Node root;

    public FileTree(String rootName) {
        root = new Node(rootName);
    }

    public Node getRoot() {
        return root;
    }

    public void add(Path path) {
        List<String> treeBranch = Arrays.stream(path.toUri().getPath().split("/")).filter(s -> !s.isEmpty()).collect(Collectors.toList());

        Node currentNode = root;
        for (String nodeName : treeBranch)
            currentNode = currentNode.addChild(nodeName);
    }

    public static class Node {
        private String name;
        private List<Node> children = new ArrayList<>();

        public Node(String name) {
            this.name = name;
        }

        public Node addChild(String nodeName) {
            Optional<Node> node = children.stream().filter(n -> n.name.equals(nodeName)).findFirst();
            if (node.isPresent())
                return node.get();

            Node newNode = new Node(nodeName);
            children.add(newNode);
            return newNode;
        }

        public List<Node> getChildren() {
            return children;
        }

        public String getName() {
            return name;
        }

    }
}
