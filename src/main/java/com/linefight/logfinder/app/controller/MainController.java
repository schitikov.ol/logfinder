package com.linefight.logfinder.app.controller;

import com.linefight.logfinder.app.common.PhraseSearcher;
import com.linefight.logfinder.app.model.FileTree;
import com.linefight.logfinder.app.service.LogfinderService;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.StyleClassedTextArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

@Controller
public class MainController {

    private static final String PHRASE_SEARCHER_PROPERTY = "phraseSearcher";
    private static final String FONT_SIZE_PROPERTY = "fontSize";
    private static final String DEFAULT_EXTENSION = "log";
    private static final int DEFAULT_FONT_SIZE = 15;

    @FXML
    private Pane pane;
    @FXML
    private TextField searchPhraseTextField;
    @FXML
    private TextField extensionTextField;
    @FXML
    private TreeView<String> treeView;
    @FXML
    private Label directoryPathLabel;
    @FXML
    private TabPane tabPane;
    @FXML
    private Button selectAllButton;
    @FXML
    private Button leftArrowButton;
    @FXML
    private Button rightArrowButton;
    @FXML
    private Button selectDirectoryButton;

    private LogfinderService logfinderService;

    private File selectedDirectory;

    private String searchPhrase;

    private ExecutorService executorService;

    private List<Path> filesContainingPhrase = new CopyOnWriteArrayList<>();

    public void paneKeyReleased(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER)
            enterKeyPressed();
        else if (keyEvent.isControlDown() && (keyEvent.getCode() == KeyCode.LEFT || keyEvent.getCode() == KeyCode.RIGHT))
            arrowKeyPressed(keyEvent.getCode());
    }

    public void paneKeyPressed(KeyEvent keyEvent) {

    }

    public void selectDirectoryButtonPressed(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose directory for searching...");
        File selectedDirectory = directoryChooser.showDialog(pane.getScene().getWindow());
        if (selectedDirectory != null) {
            this.selectedDirectory = selectedDirectory;
            directoryPathLabel.setText(selectedDirectory.getPath());
        }
    }

    public void treeViewMouseClicked(MouseEvent mouseEvent) {
        if (!mouseEvent.getButton().equals(MouseButton.PRIMARY) || mouseEvent.getClickCount() < 2) {
            return;
        }

        if (!treeView.getSelectionModel().getSelectedItem().getChildren().isEmpty()) {
            return;
        }

        selectAllButton.setDisable(false);
        leftArrowButton.setDisable(false);
        rightArrowButton.setDisable(false);

        Path file = filesContainingPhrase.stream().filter(p -> p.endsWith(treeView.getSelectionModel().getSelectedItem().getValue())).findFirst().get();

        PhraseSearcher phraseSearcher = logfinderService.getPhraseSearcherFor(searchPhrase, file);

        StyleClassedTextArea textArea = new StyleClassedTextArea();
        textArea.appendText(phraseSearcher.getText());
        textArea.setEditable(false);
        textArea.setWrapText(true);
        textArea.setStyle("-fx-font-size: " + DEFAULT_FONT_SIZE);
        VirtualizedScrollPane<StyleClassedTextArea> vp = new VirtualizedScrollPane<>(textArea);
        vp.scrollYToPixel(0.0d);

        Tab tab = new Tab(file.getFileName().toString(), vp);

        tab.setOnClosed(event -> {
            if (tabPane.getTabs().isEmpty()) {
                selectAllButton.setDisable(true);
                leftArrowButton.setDisable(true);
                rightArrowButton.setDisable(true);
            }
        });

        textArea.getProperties().put(PHRASE_SEARCHER_PROPERTY, phraseSearcher);
        textArea.getProperties().put(FONT_SIZE_PROPERTY, DEFAULT_FONT_SIZE);

        tabPane.getTabs().add(tab);
    }

    private void enterKeyPressed() {
        if (selectedDirectory == null)
            return;
        if (searchPhraseTextField.getText() == null || searchPhraseTextField.getText().isEmpty())
            return;

        searchPhrase = searchPhraseTextField.getText();

        searchPhraseTextField.setDisable(true);
        extensionTextField.setDisable(true);
        selectDirectoryButton.setDisable(true);

        executorService.submit(() -> {
            String extension = DEFAULT_EXTENSION;

            if (extensionTextField.getText() != null && !extensionTextField.getText().isEmpty())
                extension = extensionTextField.getText();

            List<Path> files = logfinderService.findFilesByExtension(selectedDirectory.getPath(), extension);
            List<Path> filesContainingPhrase = new ArrayList<>();

            for (Path path : files) {
                PhraseSearcher phraseSearcher = logfinderService.getPhraseSearcherFor(searchPhrase, path);
                if (phraseSearcher.hasMatch())
                    filesContainingPhrase.add(path);
            }

            FileTree fileTree = new FileTree("Search results");
            filesContainingPhrase.forEach(fileTree::add);

            Platform.runLater(() -> {
                treeView.setRoot(null);
                this.filesContainingPhrase.clear();
                this.filesContainingPhrase.addAll(filesContainingPhrase);

                treeView.setRoot(getTreeItemFromFileTreeNode(fileTree.getRoot()));

                searchPhraseTextField.setDisable(false);
                extensionTextField.setDisable(false);
                selectDirectoryButton.setDisable(false);
            });
        });
    }

    private TreeItem<String> getTreeItemFromFileTreeNode(FileTree.Node node) {
        TreeItem<String> treeItem = new TreeItem<>();
        treeItem.setValue(node.getName());

        for (FileTree.Node child : node.getChildren())
            treeItem.getChildren().add(getTreeItemFromFileTreeNode(child));

        return treeItem;
    }

    private void arrowKeyPressed(KeyCode code) {
        StyleClassedTextArea textArea = getTextAreaForCurrentTab();

        if (textArea == null)
            return;

        PhraseSearcher phraseSearcher = (PhraseSearcher) textArea.getProperties().get(PHRASE_SEARCHER_PROPERTY);

        textArea.clearStyle(0, textArea.getText().length());

        int matchIndex = code == KeyCode.LEFT ? phraseSearcher.prevMatchIndex() : phraseSearcher.nextMatchIndex();

        textArea.setStyleClass(matchIndex, matchIndex + searchPhrase.length(), "currentSelection");
    }

    public void selectAllButtonPressed(ActionEvent actionEvent) {
        StyleClassedTextArea textArea = getTextAreaForCurrentTab();

        if (textArea == null)
            return;

        PhraseSearcher phraseSearcher = (PhraseSearcher) textArea.getProperties().get(PHRASE_SEARCHER_PROPERTY);

        textArea.clearStyle(0, textArea.getText().length());

        for (int matchIndex : phraseSearcher.allMatchIndices())
            textArea.setStyleClass(matchIndex, matchIndex + searchPhrase.length(), "selection");
    }

    public void leftArrowButtonPressed(ActionEvent actionEvent) {
        arrowKeyPressed(KeyCode.LEFT);
    }

    public void rightArrowButtonPressed(ActionEvent actionEvent) {
        arrowKeyPressed(KeyCode.RIGHT);
    }

    @SuppressWarnings("unchecked")
    private StyleClassedTextArea getTextAreaForCurrentTab() {
        if (tabPane.getTabs().isEmpty())
            return null;
        if (tabPane.getSelectionModel().isEmpty())
            return null;

        Tab tab = tabPane.getSelectionModel().getSelectedItem();
        return ((VirtualizedScrollPane<StyleClassedTextArea>) tab.getContent()).getContent();
    }

    @Autowired
    public void setLogfinderService(LogfinderService logfinderService) {
        this.logfinderService = logfinderService;
    }

    @Autowired
    public void setExecutorService(ExecutorService executorService) {
        this.executorService = executorService;
    }
}