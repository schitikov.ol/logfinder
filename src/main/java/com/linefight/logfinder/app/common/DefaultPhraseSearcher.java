package com.linefight.logfinder.app.common;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class DefaultPhraseSearcher implements PhraseSearcher {

    private String phrase;
    private String text;
    private List<Integer> matchedIndices = new ArrayList<>();
    private int currentIndex = -1;


    public DefaultPhraseSearcher(String phrase, Path file) {
        this.phrase = phrase;

        StringBuilder stringBuilder = new StringBuilder();
        try {
            for (String line : Files.readAllLines(file)) {
                stringBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        text = stringBuilder.toString();
    }

    @Override
    public int nextMatchIndex() {
        if (!matchedIndices.isEmpty() && currentIndex < matchedIndices.size() - 1) {
            return matchedIndices.get(++currentIndex);
        }

        int matchIndex;

        if (currentIndex < 0)
            matchIndex = text.indexOf(phrase);
        else
            matchIndex = text.indexOf(phrase, matchedIndices.get(matchedIndices.size() - 1) + phrase.length());

        if (matchIndex < 0 && currentIndex >= 0) {
            return matchedIndices.get(currentIndex);
        }
        if (matchIndex < 0)
            return matchIndex;

        matchedIndices.add(matchIndex);
        currentIndex++;
        return matchIndex;
    }

    @Override
    public int prevMatchIndex() {
        if (!matchedIndices.isEmpty() && currentIndex > 0) {
            return matchedIndices.get(--currentIndex);
        }

        return currentIndex;
    }

    @Override
    public List<Integer> allMatchIndices() {

        int currentIndex = -1;

        if (this.currentIndex >= 0)
            currentIndex = this.currentIndex;

        int matchIndex = matchedIndices.isEmpty() ? -1 : matchedIndices.get(matchedIndices.size() - 1);

        int i;
        while ((i = nextMatchIndex()) != matchIndex) {
            matchIndex = i;
        }

        this.currentIndex = currentIndex;

        return new ArrayList<>(matchedIndices);
    }

    @Override
    public boolean hasMatch() {
        return text.contains(phrase);
    }

    @Override
    public String getText() {
        return text;
    }
}
