package com.linefight.logfinder.app.common;

import java.util.List;

public interface PhraseSearcher {
    int nextMatchIndex();

    int prevMatchIndex();

    List<Integer> allMatchIndices();

    boolean hasMatch();

    String getText();
}
