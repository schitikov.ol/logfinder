package com.linefight.logfinder.app;

import com.linefight.logfinder.config.RootContextConfiguration;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Logfinder extends Application {

    private ConfigurableApplicationContext springContext;
    private Parent rootNode;

    public static void main(String... args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(new Scene(rootNode));
        primaryStage.setTitle("Logfinder");
        primaryStage.show();
    }

    @Override
    public void init() throws Exception {
        springContext = new AnnotationConfigApplicationContext(RootContextConfiguration.class);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/logfinder.fxml"));
        fxmlLoader.setControllerFactory(springContext::getBean);
        rootNode = fxmlLoader.load();
    }

    @Override
    public void stop() {
        springContext.close();
    }
}
