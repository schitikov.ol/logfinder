package com.linefight.logfinder.app.service;

import com.linefight.logfinder.app.common.PhraseSearcher;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Validated
public interface LogfinderService {

    @NotNull
    default List<Path> findFilesByExtension(@NotBlank String location, @NotBlank String extension) {
        Path searchLocation = Paths.get(location);
        if (!Files.isDirectory(searchLocation))
            return Collections.emptyList();

        List<Path> files = new LinkedList<>();

        try {
            files = Files.find(searchLocation, Integer.MAX_VALUE,
                    (path, attrs) -> Files.isReadable(path) && attrs.isRegularFile() && path.toString().endsWith("." + extension)
            ).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return files;
    }

    PhraseSearcher getPhraseSearcherFor(@NotBlank String phrase, @NotNull Path path);
}
