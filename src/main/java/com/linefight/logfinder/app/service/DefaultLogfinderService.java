package com.linefight.logfinder.app.service;

import com.linefight.logfinder.app.common.DefaultPhraseSearcher;
import com.linefight.logfinder.app.common.PhraseSearcher;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class DefaultLogfinderService implements LogfinderService {

    @Override
    public PhraseSearcher getPhraseSearcherFor(String phrase, Path path) {
        if (!Files.isRegularFile(path))
            throw new IllegalArgumentException("Path must point to file.");
        if (!Files.isReadable(path))
            throw new IllegalArgumentException("File must be readable.");

        return new DefaultPhraseSearcher(phrase, path);
    }
}
