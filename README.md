# Logfinder Project

## Tools:
+ **Backend:** JDK 8, Spring 5.0.9 (Core, Context, Test)
+ **Frontend:** JavaFX 8, RichTextFX
+ **IDE:** IntelliJ IDEA 2018
+ **Building:** Gradle
+ Git

## Features:
+ Find plain text files with search phrase occurrences
+ Search phrase navigation (next, back, select all) 
+ Multiple tabs

## Building and start-up:
In the project root do the following in a console: `gradle run`. The command build and run the app.

## Using the tool:
1) Enter the search phrase
2) Choose a directory with "Directory..." button
3) Enter the file extension (default "log"). For example, if you want to look .txt files then enter "txt"
4) Press Enter
5) At the left, you can see file tree of files containing the search phrase
6) Double clicking the file name will create new tab with the file contents (multiple tabs allowed)
7) You can navigate search phrase occurrences with navigation buttons ("Select all", "<", ">") or by pressing CTRL+Left/Right

## Screenshots:
![](screenshots/logfinder.gif)